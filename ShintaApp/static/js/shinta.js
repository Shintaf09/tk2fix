function setCookie(cname, cvalue, exdays = 365) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function lihattempat() {
    if ($("#namatempatkosong").html() === "") {
        $.getJSON('/static/json/dong.json', function(result){        
            $("#namatempatkosong").html(result.namatempatkosong);
            $("#btn-tempat").html("close");
        });
    } else {
        setTimeout(() => {
            $("#namatempatkosong").html("");
            $("#btn-tempat").html("Lihat Tempat");
        }, 100);
    }
}


function infogizi() {
    if ($("#infogizikosong").html() === "") {
        $.getJSON('/static/json/dong.json', function(result){        
            $("#infogizikosong").html(result.infogizikosong);
            $("#btn-gizi").html("close");
        });
    } else {
        setTimeout(() => {
            $("#infogizikosong").html("");
            $("#btn-gizi").html("Info Gizi");
        }, 100);
    }
    
}


function lihattips() {
    if ($("#tipskosong").html() === "") {
        $.getJSON('/static/json/dong.json', function(result){        
            $("#tipskosong").html(result.tipskosong);
            $("#btn-tips").html("close");
        });
    } else {
        setTimeout(() => {
            $("#tipskosong").html("");
            $("#btn-tips").html("Lihat Tips");
        }, 100);
    }
    
}

$(document).ready(function() {
    $("#btn-tempat").click(lihattempat);
    $("#btn-tips").click(lihattips);
    $("#btn-gizi").click(infogizi);
});

$(window).load(function() {
    if (getCookie("pernahdatang") !== "sudah") {
        alert("Selamat Datang!");
        setCookie("pernahdatang", "sudah", 2);
    }
});

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest
from .views import login,logout,index
from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.contrib.auth.models import User

class ShintaUnitTest (TestCase):
	def test_apakah_ada_url_home (self):
		response = Client().get('/home/')
		self.assertEqual (response.status_code,200)
	def test_apakah_templatenya_pakai_home_html (self) :
		response = Client().get('/home/')
		self.assertTemplateUsed (response, 'home.html')
	def test_apakah_nama_fungsi_diviewsnya_index (self):
		found = resolve ('/home/')
		self.assertEqual(found.func, index)
	def test_apakah_ada_tulisan_ResepPisan (self):
		request = HttpRequest()
		response = index (request)
		html_response = response.content.decode('utf8')
		self.assertIn ('ResepPisan', html_response)
	def test_apakah_url_gaada_tidak_ada (self):
		response = Client().get('/gaada/')
		self.assertEqual (response.status_code,404)
	def test_apakah_url_login_ada(self):
		response = Client().get('/login/')
		self.assertEqual (response.status_code,200)
	def test_apakah_url_register_ada(self):
		response = Client().get('/register/')
		self.assertEqual (response.status_code,200)
	def test_apakah_url_logout_ada(self):
		response = Client().get('/logout/')
		self.assertEqual(response.status_code, 302)
	def test_login_logout(self):
		client = Client()

		username = 'kocheng'
		password = 'kocheng123'
		user = User.objects.create_user(username=username, password=password)

		# Login
		response = client.post('/login/', {
		'username': username,
		'password': password
		})

		# Test if login successful
		response = client.get('/home/')
		self.assertEqual(response.status_code, 200)
		self.assertIn('ResepPisan', response.content.decode())

		# Logout
		response = client.get('/logout/')

		# Test if logout successful
		response = client.get('/home/')
		self.assertEqual(response.status_code, 200)
	def test_register(self):
		client = Client()

		username = 'kocheng'
		email = 'kochengpaacil@gmail.com'
		password = 'kocheng123'
		user = User.objects.create_user(username=username, email=email, password=password)

		# Login
		response = client.post('/register/', {
		'username': username,
		'email' : email,
		'password': password
		})

		# Test if login successful
		response = client.get('/home/')
		self.assertEqual(response.status_code, 200)
		self.assertIn('ResepPisan', response.content.decode())

		# Logout
		response = client.get('/logout/')

		# Test if logout successful
		response = client.get('/home/')
		self.assertEqual(response.status_code, 200)



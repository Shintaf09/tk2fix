from django.urls import path, include
from django.contrib import admin
from .views import *

app_name = 'ShintaApp'

urlpatterns = [
	path('', index, name="index"),
	path('home/', index, name="index"),
	path('register/', register, name="register"),
	path('login/', login, name="login"),
	path('logout/', logout, name="logout"),
	

]
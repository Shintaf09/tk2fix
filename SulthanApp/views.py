from django.shortcuts import render, redirect

from django.views.decorators.http import require_POST

from .forms import ContactUsForm
from .models import ContactUs


# Create your views here.
def contactUsFunc(request):
    list_testi = ContactUs.objects.order_by('id')
    form = ContactUsForm()
    context = {'form' : form, 'list_testi' : list_testi}

    return render(request, 'about.html', context)

@require_POST
def tambahContactUs(request):
    form = ContactUsForm(request.POST)

    contactUsInput = ContactUs(namaInput = request.POST['nama'], emailInput = request.POST['email'], noTelpInput = request.POST['noTelp'], messageInput = request.POST['message'], bukti = request.POST['testi'])
    contactUsInput.save()

    return redirect('SulthanApp:aboutUs')

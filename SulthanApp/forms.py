from django import forms

class ContactUsForm(forms.Form):
    nama = forms.CharField(max_length=255, widget = forms.TextInput(attrs={'style' : 'font-size: 1vw', 'class' : 'form-control', 'placeholder' : 'Nama Lengkap', 'id' : 'nama'}))
    email = forms.CharField(max_length=255, widget = forms.TextInput(attrs={'style' : 'font-size: 1vw', 'class' : 'form-control', 'placeholder' : 'Email', 'id' : 'email'}))
    noTelp = forms.CharField(max_length=15, widget = forms.TextInput(attrs={'style' : 'font-size: 1vw', 'class' : 'form-control', 'placeholder' : 'Nomor Telepon', 'id' : 'noTelp'}))
    message = forms.CharField(max_length=1000, widget = forms.Textarea(attrs={'style' : 'font-size: 1vw', 'class' : 'form-control h-25','id' : 'message'}))
    testi = forms.CharField(max_length=1000, widget = forms.Textarea(attrs={'style' : 'font-size: 1vw', 'class' : 'form-control', 'id' : 'testi'}))
    
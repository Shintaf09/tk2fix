from django.db import models

# Create your models here.

class ContactUs(models.Model):
    namaInput = models.CharField(max_length=300)
    emailInput = models.CharField(max_length=300)
    noTelpInput = models.CharField(max_length=15)
    messageInput = models.CharField(max_length=300)
    bukti = models.CharField(max_length=300)    

    def __str__(self):
        return self.namaInput
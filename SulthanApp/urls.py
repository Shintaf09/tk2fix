from django.urls import path, include
from django.contrib import admin
from . import views, models, forms

app_name = 'SulthanApp'

urlpatterns = [
	path('', views.contactUsFunc, name="aboutUs"),
	path('tambahContactUs/', views.tambahContactUs, name="tambahContactUs"),
]
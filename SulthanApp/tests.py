from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
from .forms import *
from .models import *


# Create your tests here.

class sulthanAppUnitTest(TestCase):

    def test_apakah_ada_page_contact_us(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_apakah_page_contact_us_menggunakan_html_yang_tepat(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response,'about.html')

    # def test_apakah_menggunakan_fungsi_tambahContactUs(self):
    #     found = resolve('/tambahContactUs/')
    #     self.assertEqual(found.func, tambahContactUs)

    def test_return_string_model(self):
        objTest = ContactUs(namaInput = "tesnama")
        objTest1 = ContactUs(emailInput = "tesemail@gmail.com")
        objTest2 = ContactUs(noTelpInput = "1234567890")
        objTest3 = ContactUs(messageInput = "tesmessage")
        objTest4 = "tesnama"
        self.assertEqual(str(objTest), objTest4)

    # def test_post_berhasil(self):
    #     response = self.client.post('/tambahContactUs/',data={'namaInput' : 'test', 'emailInput' : 'ok@gmail.com', 'noTelpInput' : 'test', 'messageInput' : 'test'})
    #     counting_all_available_ContactUs_now = ContactUs.objects.all().count()
    #     self.assertEqual(counting_all_available_ContactUs_now, 1)
    #     self.assertEqual(response.status_code, 302)
    #     self.assertEqual(response['location'], '')
    #     new_response = self.client.get('')
    #     html_response = new_response.content.decode('utf8')
    #     self.assertIn('test', html_response)



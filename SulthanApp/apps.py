from django.apps import AppConfig


class SulthanappConfig(AppConfig):
    name = 'SulthanApp'

from django.urls import path
from . import views

urlpatterns = [
    path('upload/', views.upload, name='upload'),
    path('list_resep/', views.list_resep, name='list_resep'),
]
from django.test import TestCase, Client
from django.urls import resolve

from .views import upload
from .models import UploadModels

class UploadTest(TestCase):
    def test_upload_url_exist(self):
        response = Client().get('/upload/')
        self.assertEqual(response.status_code, 200)

    def test_using_upload_view(self):
        found = resolve('/upload/')
        self.assertEqual(found.func, upload)

    def test_using_upload_template(self):
        response = Client().get('/upload/')
        self.assertTemplateUsed(response, 'upload.html')




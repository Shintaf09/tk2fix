from django import forms
from .models import UploadModels

class UploadForms(forms.ModelForm):
    class Meta:
        model = UploadModels
        fields = ['nama_resep', 'bahan_bahan', 'langkah_buat', 'photo',]

        widgets = {
            
        }
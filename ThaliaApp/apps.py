from django.apps import AppConfig


class ThaliaappConfig(AppConfig):
    name = 'ThaliaApp'

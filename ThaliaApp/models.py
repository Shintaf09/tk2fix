from django.db import models


class UploadModels(models.Model):
    nama_resep = models.CharField(max_length=150)
    bahan_bahan = models.TextField(max_length=500)
    langkah_buat = models.TextField(max_length=500)
    photo = models.ImageField(upload_to='images/')

class UploadPhoto(models.Model):
    photo = models.ImageField(upload_to='images/')
    resep = models.ForeignKey(UploadModels, on_delete=models.CASCADE)


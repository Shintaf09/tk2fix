from django.shortcuts import render, redirect
from django.http import JsonResponse
import copy


from .models import UploadModels
from .forms import UploadForms

def upload(request):
        page_upload = UploadModels.objects.all()
        form_upload = UploadForms()
        if request.method == 'POST':
        #     newResep = copy.deepcopy(request.POST)
            form_upload = UploadForms(request.POST, request.FILES)
        #     photo_resep_form = PhotoForms(request.POST, request.FILES)
            if form_upload.is_valid():
                nama_resep = request.POST['nama_resep']
                bahan_bahan = request.POST['bahan_bahan']
                langkah_buat = request.POST['langkah_buat']
                photo = request.FILES['photo']
                new_resep = UploadModels(nama_resep=nama_resep, bahan_bahan=bahan_bahan, langkah_buat=langkah_buat, photo=photo)
                new_resep.save()
                # new_photo = PhotoForms(resep = new_resep, photo = photo_resep_form.cleaned_data['photo'])
                # new_photo.save()
                return render(request, 'upload.html', {
                    'success': True
                })
            return render(request, 'upload.html', {
                'failed': True
            })
        return render(request, 'upload.html', {
            'page_upload': page_upload, 'form' : form_upload
        })

def list_resep(request):
    resep_list = list(UploadModels.objects.values())
    return JsonResponse(resep_list, safe=False)
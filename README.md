Resep Pisan
============

[![pipeline status](https://gitlab.com/HelloImShinta/tk2fix/badges/master/pipeline.svg)](https://gitlab.com/HelloImShinta/tk2fix/commits/master)
[![coverage report](https://gitlab.com/HelloImShinta/tk2fix/badges/master/coverage.svg)](https://gitlab.com/HelloImShinta/tk2fix/commits/master)

## Nama Kelompok :
- Hilmi Arista (1806147073)
- Shinta Fauziah (1806147180)
- Thalia Teresa (1806190992)
- Sulthan Zahran Sunata (1806191985)

## Link HerokuApp :
http://reseppisan.herokuapp.com/

## Deskripsi Aplikasi dan Manfaat : 
Situs ini menyajikan informasi terlengkap mengenai segala hal tentang dunia masak-memasak. Kami berusaha menyajikan langkah-langkah yang mudah dipahami dan praktis, untuk membantu meringankan pekerjaan Anda di dapur. Cocok untuk pemula yang sedang belajar masak, maupun yang sudah mahir namun sering bingung mau masak apa.

## Daftar fitur :
- Homepage (Register, Login, logout, tips and trick, informasi gizi, top kuliner depok)
- Resep (Tampilan resep, search resep, comment)
- Upload Resep (Form upload : nama, langkah,foto)
- About Us(Foto team, testimoni)
 

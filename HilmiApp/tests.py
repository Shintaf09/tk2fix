from django.test import TestCase, Client
import unittest
from .views import *
# Create your tests here.

class HilmiUnitTest(TestCase):

	def test_apakah_url_recipe_list_ada_dan_berjalan(self):
		response = Client().get('/recipe-list/')
		self.assertEqual (response.status_code,200)
	def test_apakah_menggunakan_html_recipe_list_html(self):
		response = Client().get('/recipe-list/')
		self.assertTemplateUsed(response, 'Recipe List.html')
	def test_apakah_ada_tulisan_daftar_resep(self):
		response = Client().get('/recipe-list/')
		content = response.content.decode('utf8')
		self.assertIn("DAFTAR RESEP", content)
	
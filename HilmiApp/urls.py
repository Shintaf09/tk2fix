from django.urls import path
from django.contrib import admin
from .views import *

app_name = 'HilmiApp'

urlpatterns = [
	path('', showRecipe, name="recipe"),
]
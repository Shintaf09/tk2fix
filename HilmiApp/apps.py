from django.apps import AppConfig


class HilmiappConfig(AppConfig):
    name = 'HilmiApp'

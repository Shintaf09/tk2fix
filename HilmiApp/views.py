from django.shortcuts import render
from django.http import JsonResponse
from ThaliaApp.models import UploadModels


# Create your views here.

def showRecipe(request):
	uploadModels = UploadModels.objects.all()
	#content = {
	#	'listResep' : uploadModels,
	#	'listFoto' : uploadPhoto,
	#}
	return render (request, "Recipe List.html", {
		'uploadModels': uploadModels
	})

# def get_data(request, key):
# 	url = 'https://www.googleapis.com/books/v1/volumes?q=' + key
# 	response = requests.get(url)
# 	response_json = response.json()

# 	return JsonResponse(response_json)
